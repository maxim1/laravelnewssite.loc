<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = 'comments';

    protected $primaryKey = 'id';


    protected $fillable = [
        'newsId', 'commenterName', 'commentText',
    ];

    public function news (){
        return $this->belongsTo('App\News','id', 'newsId');
    }

}
