<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
   

    public function show($id){

        $news = News::find($id);
        $comments = $news->comments;

        return view('textNews', [
            'news'=>$news,
            'comments'=>$comments
        ]);

    }
}
