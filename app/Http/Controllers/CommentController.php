<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class CommentController extends Controller
{
    //
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publishComment (Request $request){
        $comment = new Comment();
        $comment->newsId = $request->input('newsId');
        $comment->commenterName = $request->input('commentatorName');
        $comment->commentText = $request->input('textComment');

        if (!$comment->save()) {
            return response()->json(['comResult' => ''], 200);

        }else {
            return response()->json( $comment, 200);

        }
    }
}
