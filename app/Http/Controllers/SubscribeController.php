<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;


class SubscribeController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {

        $subscriber = Subscriber::where('email', 'LIKE', $request->input('email'))->get();

        if (!$subscriber->isEmpty()) {
            $subscribeResult = 'Подписка на адрес '. $request->input('email') .' уже оформлена ранее';
            return response()->json(['subscribeResult' => $subscribeResult], 200);
        } else {

            $newSubscriber = new Subscriber;
            $newSubscriber->email = $request->email;
            $newSubscriber->save();
            $subscribeResult = 'Подписка на адрес ' . $request->input('email') . ' оформлена';
            return response()->json(['subscribeResult' => $subscribeResult], 200);

        }

    }


}


