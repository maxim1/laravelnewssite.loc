<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class News extends Model
{
    protected $table = 'news';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'summary', 'fulltext', 'images',
    ];

    public function comments (){
        return $this->hasMany('App\Comment','newsId', 'id');
    }

    //
}
